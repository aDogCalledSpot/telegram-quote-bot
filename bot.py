import yaml
from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    BigInteger,
    String,
    Date,
    DateTime,
    create_engine,
    select,
)
import random
from sqlalchemy.sql import func
from sqlalchemy.orm import DeclarativeBase, relationship, sessionmaker, scoped_session
from telegram import Poll
from telegram.ext import CommandHandler, Application, ApplicationBuilder
from telegram.error import TelegramError
import re
import datetime as dt

class Base(DeclarativeBase):
    pass


class Quote(Base):
    __tablename__ = "quotes"
    gid = Column(BigInteger, primary_key=True, nullable=False)
    message_id = Column(Integer, primary_key=True, nullable=False)
    quote = Column(String(512), primary_key=False, nullable=False)
    account_id = Column(Integer, primary_key=False, nullable=False)
    date = Column(Date, primary_key=False, nullable=False)
    last_quoted = Column(DateTime, primary_key=False, nullable=False)

async def post_init(application: Application) -> None:
    await application.bot.set_my_commands(
        [
            ("start", "Start the bot"),
            ("quote", "Quote a message to save it for later use"),
            ("unquote", "Unquote boring messages"),
            ("embarrass", "Bring up an old quoted message by that user"),
            ("quotequiz", "Guess whose quote it is")
        ]
    )

async def start(update, context):
    chat_id = update.effective_chat.id
    if chat_id >= 0:
        text = "Hello, you should probably add me to a group chat."
    else:
        text = "Try quoting a message by replying to it with /quote."
    await context.bot.send_message(chat_id=chat_id, text=text)


def sanitize_markdown(message, use_caption=False):
    if use_caption:
        if len(message.caption_entities) == 0:
            return message.caption
        message_text = message.caption
        entities = message.caption_entities
    else:
        if len(message.entities) == 0:
            return message.text
        message_text = message.text
        entities = message.entities

    # Replaces @ at the beginning of a word with ＠
    message_text = re.sub(r'(\s)@', r'\1＠', message_text)

    # Guarantee that they are in the correct order
    entities = sorted(entities, key=lambda x: x.offset)
    quote = message_text[: entities[0].offset]
    for i in range(len(entities)):
        entity = entities[i]
        start = entity.offset
        end = entity.offset + entity.length
        if i + 1 < len(entities):
            start_next_entity = entities[i + 1].offset
        else:
            start_next_entity = None

        if entity.type == "text_link":
            quote += f'<a href="{entity.url}">{message_text[start:end]}</a>{message_text[end:start_next_entity]}'
            continue
        elif entity.type == "bold":
            tag = "b"
        elif entity.type == "italic":
            tag = "i"
        elif entity.type == "strikethrough":
            tag = "s"
        elif entity.type == "underline":
            tag = "u"
        elif entity.type == "code":
            tag = "code"
        elif entity.type == "pre":
            tag = "pre"
        elif entity.type == "spoiler":
            tag = "tg-spoiler"
        else:
            quote += message_text[start:start_next_entity]
            continue

        quote += f"<{tag}>{message_text[start:end]}</{tag}>{message_text[end:start_next_entity]}"
    return quote


def get_message_url(gid, mid):
    clean_gid = str(gid)[4:]
    return f"https://t.me/c/{clean_gid}/{mid}"


async def quote(update, context):
    chat_id = update.effective_chat.id
    quote_message = update.message.reply_to_message
    reply_message = update.message.message_id

    if not quote_message:
        text = "You need to reply to a message when using /quote."
        await context.bot.send_message(
            chat_id=chat_id, reply_to_message_id=reply_message, text=text
        )
        return

    if quote_message.from_user.is_bot:
        text = "Bots have nothing interesting to say."
        await context.bot.send_message(
            chat_id=chat_id,
            reply_to_message_id=reply_message,
            text=text,
        )
        return

    quote_poster_uid = quote_message.from_user.id
    if quote_poster_uid == update.message.from_user.id:
        text = "Quoting yourself? Really?"
        await context.bot.send_message(
            chat_id=chat_id, reply_to_message_id=reply_message, text=text
        )
        return

    if not quote_message.text or quote_message.text == "":
        if quote_message.caption and quote_message.caption != "":
            quote = sanitize_markdown(quote_message, use_caption=True)
        else:
            text = "There should be something to quote in the message."
            await context.bot.send_message(
                chat_id=chat_id, reply_to_message_id=reply_message, text=text
            )
            return
    else:
        quote = sanitize_markdown(quote_message)

    with DBSession() as session:
        # Check for identical quotes
        existing_quote = (
            session.query(Quote)
            .filter_by(account_id=quote_poster_uid, quote=quote)
            .first()
        )
        if existing_quote is not None:
            if existing_quote.message_id == quote_message.message_id:
                text = "This message is already in the database."
            else:
                text = f'An identical quote by this user already exists <a href="{get_message_url(existing_quote.gid, existing_quote.message_id)}">here</a>.'
            await context.bot.send_message(chat_id=chat_id, text=text, parse_mode="HTML")
            return

        quote = Quote(
            gid=chat_id,
            message_id=quote_message.message_id,
            account_id=quote_poster_uid,
            quote=quote,
            date=quote_message.date,
            last_quoted=quote_message.date,
        )
        session.add(quote)
        session.commit()

    text = "Message saved."
    await context.bot.send_message(
        chat_id=chat_id, reply_to_message_id=reply_message, text=text
    )


async def embarrass(update, context):
    chat_id = update.effective_chat.id
    reply_message = update.message.message_id

    if not update.message.reply_to_message:
        text = "You need to reply to a message, so I know who to embarrass."
        await context.bot.send_message(
            chat_id=chat_id, text=text, reply_to_message_id=reply_message
        )
        return

    if update.message.reply_to_message.from_user.is_bot:
        text = "Bots don't embarrass themselves."
        await context.bot.send_message(
            chat_id=chat_id,
            reply_to_message_id=reply_message,
            text=text,
        )
        return

    embarrass_uid = update.message.reply_to_message.from_user.id
    if embarrass_uid == update.message.from_user.id:
        text = "Why embarrass yourself? Have some self respect."
        await context.bot.send_message(
            chat_id=chat_id,
            reply_to_message_id=reply_message,
            text=text,
        )
        return

    with DBSession() as session:
        quotes = session.query(Quote).filter_by(account_id=embarrass_uid).order_by(Quote.last_quoted).all()
        if len(quotes) == 0:
            text = "No quotes for this user yet."
            await context.bot.send_message(chat_id=chat_id, text=text, reply_to_message_id=update.effective_message.message_id)
            return

        if len(quotes) == 1:
            quote = quotes[0]
        else:
            max_quote_index = round(0.3 * len(quotes))
            quote = quotes[random.randrange(0, max_quote_index)]
        quote.last_quoted = dt.datetime.now()
        session.commit()
        name = (await context.bot.get_chat_member(chat_id, embarrass_uid)).user.first_name
        mention = f'<a href="tg://user?id={embarrass_uid}">{name}</a>'
        try:
            chat = (await context.bot.get_chat(quote.gid)).title
        except TelegramError:
            chat = "Unknown Group"
        message_url = get_message_url(quote.gid, quote.message_id)
        text = f'"{quote.quote}"\n    -{mention}, ({quote.date.year}), {chat}, <a href="{message_url}">Telegram</a>'
    await context.bot.send_message(
        chat_id=chat_id,
        text=text,
        reply_to_message_id=update.effective_message.message_id,
        parse_mode="HTML",
    )


async def delete_quote(update, context):
    chat_id = update.effective_chat.id
    quote_message = update.message.reply_to_message
    reply_message = update.message.message_id

    if not quote_message:
        text = "You need to reply to a quoted message when using /delete_quote."
        await context.bot.send_message(
            chat_id=chat_id, reply_to_message_id=reply_message, text=text
        )
        return

    with DBSession() as session:
        quote = (
            session.query(Quote)
            .filter_by(gid=chat_id, message_id=quote_message.message_id)
            .first()
        )

        if quote is None:
            text = "This message wasn't quoted."
            await context.bot.send_message(
                chat_id=chat_id, reply_to_message_id=reply_message, text=text
            )
            return

        quote_poster_uid = quote_message.from_user.id
        if quote_poster_uid == update.message.from_user.id:
            text = "Haha! You wish! This will be remembered forever!"
            await context.bot.send_message(
                chat_id=chat_id, reply_to_message_id=reply_message, text=text
            )
            return

        session.delete(quote)
        session.commit()

    text = "Message deleted."
    await context.bot.send_message(
        chat_id=chat_id, reply_to_message_id=reply_message, text=text
    )

MAX_QUIZ_OPTIONS = 5

async def quotequiz(update, context):
    chat_id = update.effective_chat.id
    wait_message = await context.bot.send_message(
        chat_id=chat_id,
        reply_to_message_id=update.message.message_id,
        text="Hmmmm let me think...",
    )

    with DBSession() as session:
        all_users = session.execute(
            select(Quote.account_id).filter_by(gid=chat_id).distinct()
        ).all()
        random.shuffle(all_users)
        quoted_members = [x[0] for x in all_users]

        members_in_chat: list[ChatMember] = []
        for member_id in quoted_members:
            try:
                member_data = await context.bot.get_chat_member(
                    update.effective_chat.id, member_id
                )
                if member_data.status != "left":
                    members_in_chat.append(member_data)
            except:
                pass

        if len(members_in_chat) < 2:
            await context.bot.send_message(
                chat_id=chat_id,
                reply_to_message_id=update.message.message_id,
                text="At least two people need to have been quoted.",
            )
            await wait_message.delete()
            return

        members_in_chat = members_in_chat[:MAX_QUIZ_OPTIONS]
        selected_member = random.choice(members_in_chat)
        selected_quote =  session.execute(
            select(Quote)
                .filter_by(account_id=selected_member.user.id)
                .order_by(func.random())
                .limit(1)
            ).first()[0]

    if not selected_quote:
        await context.bot.send_message(
            chat_id=chat_id,
            reply_to_message_id=update.message.message_id,
            text="Something has gone substantially wrong. Oh well.",
        )
        await wait_message.delete()
        return

    poster_link = f'<a href="tg://user?id={selected_member.user.id}">{selected_member.user.first_name}</a>'
    message_url = get_message_url(selected_quote.gid, selected_quote.message_id)
    print(message_url)

    other_options = [x for x in members_in_chat if x.user.id != selected_member.user.id]
    correct_index = members_in_chat.index(selected_member)

    # Yeah, the following shouldn't be necessary, is just a hacky way to fix the formatting
    formatted_quote = "\n".join(
        [
            "Who would say this?",
            "",
            *('"' + selected_quote.quote + '"').split("\n"),
        ]
    )
    await context.bot.send_poll(
        update.effective_chat.id,
        formatted_quote,
        [x.user.first_name for x in members_in_chat],
        is_anonymous=False,
        type=Poll.QUIZ,
        correct_option_id=correct_index,
        explanation=f'Yeah, {poster_link} actually <a href="{message_url}">said this</a>...',
        explanation_parse_mode="HTML",
    )

    await wait_message.delete()

if __name__ == "__main__":
    with open("config.yaml") as config_file:
        config = yaml.safe_load(config_file)
        application = ApplicationBuilder().token(config["token"]).post_init(post_init).build()
        db_engine = create_engine(config["db_host"], pool_pre_ping=True)
        session_factory = sessionmaker(bind=db_engine)
        DBSession = scoped_session(session_factory)
        Base.metadata.create_all(db_engine)

    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("quote", quote))
    application.add_handler(CommandHandler("embarrass", embarrass))
    application.add_handler(CommandHandler(["delete_quote", "unquote"], delete_quote))
    application.add_handler(CommandHandler("quotequiz", quotequiz))
    application.run_polling()
